<h2 align="center">Cart App</h2>


<p align="left">This program is basically a 'Cart' App, a place where you can add items (with its amount) and keep track of them.</p>
   
Through this app, you can simply create a Cart, add as many items as you want, and in the end
you can save them in a file (JSON) where you can retrieve them later. By the way, you can also create as many carts as you want!!

This App was built using Object-Oriented Programmimg (OOP) concepts (as part of my Python's studies :) )

I have also implemented a hash algorithm to check JSON integrity every time the file is loaded, then, 
if the file was modified externally (even a minor byte), the file check step will fail and the JSON file won't be loaded

Author: Rafael Santos

If you find any bugx, issuex or even improvements, merge requests are welcome.