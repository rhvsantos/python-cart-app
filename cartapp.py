from functools import total_ordering
from time import sleep
import os
from sys import exit, argv
import json
from pathlib import Path
import hashlib

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

@total_ordering
class Item:
    def __init__(self, item_name, qty):
        self.name = item_name
        self.amount = qty

    @property
    def name(self):
        return self.__name

    @property
    def amount(self):
        return self.__amount

    @amount.setter
    def amount(self,qty):
        
        if not isinstance(qty, int):
            raise ValueError('Qty must be an integer.')
        else:
            if 0 < qty < 100:
                self.__amount = qty
            else:
                raise Exception('Item amount value must be between 1 and 99.')

    @name.setter
    def name(self,item_name):
        
        if not isinstance(item_name, str):
            raise ValueError('Item name must be a String.')
        else:
            self.__name = item_name.strip().capitalize()
    
    def __str__(self):
        return f'{self.__amount} - {self.__name}'
    
    def __eq__(self, item_obj):
        if not isinstance(item_obj, Item):
            item_obj = Item(item_obj, 0)

        return self.name.casefold() == item_obj.name.casefold()
    
    def __lt__(self, item_obj):
        if not isinstance(item_obj, Item):
            item_obj = Item(item_obj, 0)

        return self.name.casefold() < item_obj.name.casefold()
    
    def __eq__(self, item_obj):
        if not isinstance(item_obj, Item):
            item_obj = Item(item_obj, 0)

        return self.name.casefold() == item_obj.name.casefold()
    
    def __repr__(self) -> str:
        return f'<Class {type(self).__name__} on 0x{id(self):x} Name: {self.__name} Amount: {self.__amount}>'


class Cart:

    @staticmethod
    def show_data(item_list):
        max_lenght = 15
        if len(item_list) != 0:
            print(f'\n{"Items List":*^15}\n')
            print(f'Amount\t\tItem')
            print('-' * 35)
            for item in item_list:
                itemName = item.name
                itemAmount = item.amount
                itemNameTruncated = itemName[:max_lenght] + ('...' if len(itemName) > max_lenght else '')
                print(f'({itemAmount:02})\t\t{itemNameTruncated}')
                if len(itemName) != len(itemNameTruncated):
                    print(f'\t\t{itemName[max_lenght:]}')
        else:
            print('Cart is empty!')

        print()
        input('Press a key to continue.')

    @staticmethod
    def return_item_collection(object_list):

        item_collection = {
            item_name: item_amount
            for item in object_list
            if isinstance(item, Item)
            if (item_name := item.name)
            if (item_amount := item.amount)
        }

        return item_collection

    def __init__(self, id=None):
        self.__items = []
        self.id = id

    @staticmethod
    def get_hash(json_data: json) -> str:
        hash_generator = hashlib.sha256()

        # String encode
        encoded_string = json_data.encode('utf-8')

        # Creating the hash
        hash_generator.update(encoded_string)

        # Getting the hash
        hash = hash_generator.hexdigest()
        
        return hash[:10]

    @property
    def id(self):
        return self.__id
    
    @id.setter
    def id(self, id):
        if isinstance(id, str) or id is None:
            self.__id = id
        else:
            raise ValueError('Cart ID must be a string value (Hash).')

    def add_item(self, item):
        if not isinstance(item, Item):
            raise TypeError('item argument must be an Item Instance.')

        returned_index = self.searchItem(item)

        if returned_index is None:
            self.__items.append(item)
            print(f'{item.name} was successfully added to your cart.')
            sleep(0.5)
        else:
            print(f'{item.name} already exists in your cart!')
            sleep(2)
        
    def remove_item(self, item):
        if not isinstance(item, Item):
            item = Item(item, 1)

        returned_index = self.searchItem(item)

        if returned_index is not None:
            itemObj = self.__items[returned_index]
            self.__items.remove(itemObj)
            print(f'{item.name} was successfully removed from your cart.')
            sleep(2)
        else:
            print(f'This Item ({item.name}) is not in the cart.')
            sleep(2)

    def sort_items(self, sort_key) -> None:

        item_vars = [attr for attr in vars(Item) if not attr.startswith('__')]

        if sort_key in item_vars:
            if sort_key == 'amount':
                _reverse=True
            else:
                _reverse=False
            self.__items.sort(key=lambda item_inst: getattr(item_inst, sort_key), reverse=_reverse)
            print('The cart was sorted!')
            sleep(1)
        else:
            print('Invalid key.')
            sleep(1)

    def list_items(self):
        Cart.show_data(self.__items)

    def cart_info(self) -> dict:

        return {'cart_id': self.id, 'content': [Cart.return_item_collection(self.__items)]}

    def json_serialize(self) -> json:

        # Gathers all items
        cart_items = self.cart_info()
        
        # Parse dict to json string
        json_data = json.dumps(cart_items)

        # Return JSON string
        return json_data
    
    def check_integrity(self, cart_id: str) -> bool:
        print('Checking file integrity...')
        sleep(1)

        get_hash = self.set_cart_id_hash()

        return get_hash == cart_id

    def set_cart_id_hash(self):
        
        cart_items_dict = Cart.return_item_collection(self.__items)

        return Cart.get_hash(json.dumps(cart_items_dict))
    
    def load_cart(self, cart_dict: dict) -> bool:
        print('Loading items into the cart..')
        sleep(1)

        # Load all items instantiating a Item class for each one
        for item_name, item_amount in cart_dict['content'][0].items():
            self.add_item(Item(item_name, item_amount))

        is_json_of = self.check_integrity(cart_dict['cart_id'])

        if is_json_of:
            print('Integrity [OK]')
            print('Done. All items were successfuly loaded!')
            self.id = self.set_cart_id_hash()
            sleep(2)
            return True
        else:
            print('The integrity check has failed.')
            print('[ERROR] The hashes don\'t match. Cart could not be loaded!')
            self.__items = []
            input()
            return False

    def __len__(self):
        return len(self.__items)
    
    def __getitem__(self, item):
        return self.__items[item]
    
    def __iter__(self):
        return iter(self.__items)
    
    def searchItem(self, item):
        if not isinstance(item, Item):
            item = Item(item, 0)

        if item in self.__items:
            item_index = self.__items.index(item)
            return item_index
        else:
            return None
        
class Menu():
    def __init__(self, title, options_list, quit_opt_name=None):
        self.title = title
        self.options_list = options_list
        self.selected_opt = None
        self.quit_opt_name = quit_opt_name

        if self.quit_opt_name is not None:
            self.options_list.insert(0, self.quit_opt_name)

    @property
    def quit_opt_name(self):
        return self.__quit_opt_name
    
    @quit_opt_name.setter
    def quit_opt_name(self, opt_name):
        if opt_name and isinstance(opt_name, str):
            self.__quit_opt_name = opt_name
        elif opt_name is None:
            self.__quit_opt_name = None
        else:
            raise TypeError('opt_name must be a string.')

    @property
    def selected_opt(self):
        return self.__selected_opt

    @property
    def title(self):
        return self.__title
    
    @property
    def options_list(self):
        return self.__options_list
    
    @title.setter
    def title(self, title):
        if title:
            self.__title = title
        else:
            raise ValueError('Title must be an string.')

    @options_list.setter
    def options_list(self, opt_list):

        if not isinstance(opt_list, list):
            raise TypeError('Option list must be a list.')
        
        if len(opt_list) == 0:
            raise Exception('Option List cannot be empty.')
        
        self.__options_list = opt_list
        
    @selected_opt.setter
    def selected_opt(self, opt):

        if opt is not None and isinstance(opt, str):
            if opt.isdigit():
                opt = int(opt)

        if isinstance(opt, int):
            if 0 <= opt < len(self):
                self.__selected_opt = opt
            else:
                print(f'Selected option is out of range.')
                sleep(1)
        elif opt is None:
                self.__selected_opt = None
        else:
            print(f'Invalid option ({opt})')
            sleep(1)
            self.__selected_opt = None     
    
    @selected_opt.deleter
    def selected_opt(self):
        del self.__options_list

    def execute(self):
        while True:
            self.selected_opt = None
            print(f'{self.title:*^15}')
            print()
           
            for ind, opt_desc in enumerate(self.__options_list[1:] if self.__quit_opt_name is not None else self.__options_list):
                opt_human_index = ind + 1
                print(f'[{opt_human_index}] - {opt_desc}')

            if self.__quit_opt_name:
                print()
                print(f'[0] - {self.__quit_opt_name}')
                print()

            opt = input('Select an option above: ')

            if self.__quit_opt_name is None:
                opt -= 1

            self.selected_opt = opt

            if self.selected_opt is not None:
                break

    def add_opt(self, opt):
        
        if opt and not opt in self.__options_list:
            self.__options_list.append(opt)
        else:
            print(f'Either this option ({opt}) already exists or is invalid.')

    def del_opt(self, opt):
        
        if opt in self.__options_list:
            self.__options_list.remove(opt)
        else:
            print(f'Seems that this option ({opt}) does not exist and cannot be deleted.')

    def __len__(self):
        return len(self.__options_list)
    
    def __repr__(self) -> str:
        return f'<Class {type(self).__name__} on 0x{id(self):x} Menu Title: {self.title} Lenght: {len(self)} Selected Option: {self.selected_opt}'
    
    def __iter__(self):
        return iter(self.__options_list)
    
    def __str__(self):
        return self.__title
    
    def __getitem__(self, object_index):
        return self.options_list[object_index]

class CartApp():

    @staticmethod
    def ask_item_name() -> str:

        while True:
            item_name = input('Item name: ')

            if 1 < len(item_name.strip()) <= 33:
                return item_name
            else:
                print('item name must contain at least 2 characters up to 33 characters.')
                continue
            
    @staticmethod
    def get_saved_carts(filepath: str) -> GeneratorExit:
        
        file_path = Path(filepath)

        if file_path.is_dir():
            jsonlocation = file_path
        else:
            print(f'"{file_path}" Path is invalid.')
            return None

        # Getting all files ending with '.json'
        json_files = jsonlocation.glob('*.json')

        return json_files
            
    @staticmethod
    def check_filepath(path_obj: Path) -> Path:
        
        if path_obj.is_dir():
            print('Filename must be a file and not a folder')
            sleep(1)
            return False

        if not path_obj.parent.exists():
                print(f'The path {path_obj.parent} does not exist.')
                sleep(1)
                return False

        #if filename.suffix != '.json':
            #filenameWithoutSuffix = filename.stem
            #filename = Path(f"{filenameWithoutSuffix}.json")
        filename = path_obj.with_suffix('.json')

        if filename.exists():
            print(f'The file {filename.name} already exists.')
            return False
        
        return filename
            
    @staticmethod
    def ask_filename() -> Path:

        while True:
            print('Enter 0 to cancel.')
            filename = input('JSON Filename: ')

            if not filename:
                continue
            if filename == '0':
                return 0

            # Performing some checks on the filename
            file = CartApp.check_filepath(Path(filename))

            if file:
                return file
            else:
                continue

    @staticmethod
    def ask_item_amount() -> int:

        while True:
            try:
                amount = int(input('Amount: '))
            except ValueError:
                print('The amount value is invalid.')
                continue
            else:
                if 0 < amount < 100:
                    return amount
                else:
                    print('Item amount value must be between 1 and 100.')
                    continue

    @staticmethod
    def save_to_file(json_data: str, filename: Path) -> bool:

        with filename.open('w') as output_file:
            output_file.write(json_data)

        # Check if the file was saved correctly
        if CartApp.load_json_file(filename, file_save_check=True):
            print('The file was successfully saved!')
            sleep(2)
            return True
        else:
            print('Something went wrong while trying to save the file.')
            sleep(2)
            return False

    @staticmethod
    def json_simple_check(json_parse: dict) -> bool:
        if 'cart_id' in json_parse and 'content' in json_parse:
            if json_parse['cart_id'] and json_parse['content']:
                return True
        return False

    @staticmethod
    def load_json_file(filename: str, file_save_check=False) -> dict:
        jsonFile = Path(filename)

        if jsonFile.exists():
            with jsonFile.open('r') as file:
                try:
                    json_parse = json.load(file)
                except json.JSONDecodeError:
                    if file_save_check:
                        return False
                    print('Unable to open the JSON file.')
                    sleep(1)
                    return False
                else:
                    if file_save_check:
                        return True
                    json_validation = CartApp.json_simple_check(json_parse)
                    
                    if json_validation:
                        print('The JSON file was successfully loaded!')
                        return json_parse
                    else:
                        print('The JSON File  could not have its integrity checked and could not be validated.')
                        return False
        else:
            if file_save_check:
                return False
            print(f'The file "{filename}" does not exist.')
            return False


    def __init__(self, base_directory):
        self.main_options = [('Create a new cart', self.new_cart), ('Load an existent cart', self.load_saved_cart)]
        self.main_menu = Menu('Cart App', [i[0] for i in self.main_options], quit_opt_name='Quit')
        self.json_base_directory = base_directory if isinstance(base_directory, str) else base_directory[0]
        self.opened_file = None

    @property
    def json_base_directory(self):
        return self.__json_base_directory
    
    @json_base_directory.setter
    def json_base_directory(self, directory_path):
        if directory_path and isinstance(directory_path, str):

            dir_path_obj = Path(directory_path)

            if dir_path_obj.is_dir() and dir_path_obj.exists():
                self.__json_base_directory = dir_path_obj
            else:
                #raise Exception('<base_directory> doesn\'t exist or is not a directory.')
                dir_path_obj.mkdir()
        else:
            raise TypeError('<base_directory> must be an string.')  

    def sort_cart(self):

        if len(self.cart) < 2:
            print('Your cart either is empty or doesn\'t have enough items.')
            sleep(1)
            return
        
        sort_menu = Menu('Sort Cart Items', ['Sort by number of items', 'Sort by item name'], quit_opt_name='Back')

        sort_menu.execute()

        if sort_menu.selected_opt == 0 and sort_menu.quit_opt_name is not None:
            return    

        selected_opt = sort_menu.options_list[sort_menu.selected_opt]

        if selected_opt == 'Sort by number of items':
            sort_key = 'amount'
        elif selected_opt == 'Sort by item name':
            sort_key = 'name'
        else:
            print('The key is invalid')
            return
        
        self.cart.sort_items(sort_key)

    def load_saved_cart(self):
        cls()
        # Get all existent json files
        all_json_files = CartApp.get_saved_carts(self.json_base_directory)

        while True:

            if all_json_files:
                # Create a list with all json filenames
                json_file_list = [file.name for file in all_json_files]

                if len(json_file_list) == 0:
                    # There is no json file
                    print("You don't have a saved cart yet, you must create and save one before.")
                    sleep(2)
                    json_filename = None
                    break

                # Instantiating a Menu object
                files_menu = Menu('Select a file below', json_file_list, quit_opt_name='Back')

                files_menu.execute()

                if files_menu.selected_opt == 0 and  files_menu.quit_opt_name is not None:
                    json_filename = None
                    break
                elif files_menu.quit_opt_name is not None:
                    json_filename = json_file_list[files_menu.selected_opt]
                    break
                else:
                    json_filename = json_file_list[files_menu.selected_opt - 1]
                    break
            else:
                json_filename = None
                break

        # If json_filename is not None
        if json_filename:
            # Build the filename string with its relative Path defined at 'self.json_base_directory'
            file_to_load = f'{self.json_base_directory}/{json_filename}'
            # Calls 'load_json_file' static method, it will return a dict if the file could be loaded successfully
            loaded_items_dict = CartApp.load_json_file(file_to_load)

            if not loaded_items_dict:
                # If the staticmethod has returned 'False'
                print('ERROR: The cart could not be loaded.')
            else:
                # Load the cart checking the json integrity
                self.cart = Cart()
                self.cart.load_cart(loaded_items_dict)

                if self.cart.id:
                    self.opened_file = Path(file_to_load)
                    self.my_cart_menu()
                else:
                    print('File could not be loaded!')
                    return
    def save_current_cart_without_quitting(self):
        self.save_current_cart(quit=False)
        return
    
    def save_current_cart(self, quit=True):
        
        if len(self.cart) == 0:
            print('Your cart is empty!')
            sleep(1)
            return
        else:

            if self.opened_file is not None:
                filename = self.opened_file.name
            else:
                filename = CartApp.ask_filename()

            if filename == 0:
                print('Operation has been canceled.')
                sleep(2)
                return
            
            full_file_path = Path(f'{self.json_base_directory}') / filename
            
            if filename:
                # Set Cart ID
                self.cart.id = self.cart.set_cart_id_hash()
                # Get JSON string
                json_data = self.cart.json_serialize()

                # Save to file
                if CartApp.save_to_file(json_data, full_file_path):

                    # Exit the application
                    self.execute() if quit == True else self.my_cart_menu
                    
            else:
                print('Cart was not saved!')
                return

    def add_cart_item(self):
        
        item_name = CartApp.ask_item_name()
        item_amount = CartApp.ask_item_amount()

        new_item = Item(item_name, item_amount)

        self.cart.add_item(new_item)

    def remove_cart_item(self):

        if len(self.cart) == 0:
            print('Your cart is empty!')
            sleep(1)
            return
        else:
            item_name = CartApp.ask_item_name()
            self.cart.remove_item(item_name)

    def show_cart_items(self):
        self.cart.list_items()

    def new_cart(self):
        # Create an empty cart
        self.cart = Cart()

        self.my_cart_menu()
        self.opened_file = None

    def my_cart_menu(self):

        while True:
            cls()
            print(f"Cart ID: {self.cart.id}" if self.cart.id else 'Cart is not saved.')
            print(f"Current File: {self.opened_file.name}\n" if self.opened_file else '')

            self.cart_options = [
            ('Add a new item', self.add_cart_item), 
            ('Remove an item', self.remove_cart_item), 
            ('Show cart items', self.show_cart_items),
            ('Sort the cart items', self.sort_cart),
            ('Save', self.save_current_cart_without_quitting),
            ('Save and Quit', self.save_current_cart)
            ]

            self.cart_menu = Menu(
                'My Cart', 
                [self.cart_options[0][0]] if len(self.cart) == 0 else [i[0] for i in self.cart_options],
                quit_opt_name='Back')
            self.cart_menu.execute()

            if self.cart_menu.selected_opt == 0 and self.cart_menu.quit_opt_name is not None:
                break

            elif self.cart_menu.quit_opt_name is not None:
                self.cart_options[self.cart_menu.selected_opt - 1][1]()
            else:
                self.cart_options[self.cart_menu.selected_opt][1]()

    def execute(self):
        while True:
            self.opened_file = None
            cls()
            self.main_menu.execute()

            if self.main_menu.selected_opt == 0 and self.main_menu.quit_opt_name is not None:
                exit('Bye!')
            elif self.main_menu.quit_opt_name is not None:
                self.main_options[self.main_menu.selected_opt - 1][1]()
            else:
                self.main_options[self.main_menu.selected_opt][1]()

if __name__ == '__main__':
    
    import argparse

    parser = argparse.ArgumentParser(
        prog='Cart App',
        description='''This program is basically a \'Cart\' App, a place where you can add items (with its amount) and keep track of them.
        
        Through this app, you can simply create a Cart, add as many items as you want, and in the end
        you can save them in a file (JSON) where you can retrieve them later. By the way, you can also create as many carts as you want.

        This App was built using Object-Oriented Programmimg (OOP) concepts (as part of my Python's studies :)).

        I have also implemented a hash algorithm to check JSON integrity every time the file is loaded, then, 
        if the file was modified externally (even a minor byte), the file check step will fail and the JSON file won't be loaded.

        Author: Rafael Santos
        ''',
        epilog='If you find any bug or issue, please feel free to report it to me (E-mail: rafael@rhvds.com.br).',
        usage='python cartapp.py [-d <Base Directory>]'
    )
    parser.add_argument('-d', '--base-directory', 
                        default='./saved-carts', 
                        required=False, 
                        nargs=1, 
                        type=str, 
                        help='Use this option to set another place to save your JSON files (default: ./saved-carts)',
                        action='store'
                        )
    parser.add_argument('-v', '--version', action='version', version='CartApp 1.0')
    args = parser.parse_args()

    app_instance = CartApp(base_directory=args.base_directory)
    app_instance.execute()